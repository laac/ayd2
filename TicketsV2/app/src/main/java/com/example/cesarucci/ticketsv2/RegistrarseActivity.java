package com.example.cesarucci.ticketsv2;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RegistrarseActivity extends AppCompatActivity {

    private AutoCompleteTextView mNombreUsuarioView;
    private AutoCompleteTextView mNombresView;
    private AutoCompleteTextView mApellidosView;
    private AutoCompleteTextView mEmailView;
    private EditText mContraseniaView;
    private EditText mContrasenia2View;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);

        mNombreUsuarioView = (AutoCompleteTextView) findViewById(R.id.usuario);
        mNombresView = (AutoCompleteTextView) findViewById(R.id.nombre);
        mApellidosView = (AutoCompleteTextView) findViewById(R.id.apellido);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mContraseniaView = (EditText) findViewById(R.id.password);
        mContrasenia2View = (EditText) findViewById(R.id.password2);

        Button registrar = (Button) findViewById(R.id.email_sign_in_button);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });
    }

    private void registrar() {
        String usuario = mNombreUsuarioView.getText().toString();
        String nombres = mNombresView.getText().toString();
        String apellidos = mApellidosView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mContraseniaView.getText().toString();

        new CargarDatos().execute("http://104.196.148.67/registro.php?nombre_usuario="+usuario+"&nombres="+nombres+"&apellidos="+apellidos+"&email="+email+"&password="+password);
    }

    private String downloadUrl(String myurl) throws IOException {
        Log.i("URL",""+myurl);
        myurl = myurl.replace(" ","%20");
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("respuesta", "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

    private class CargarDatos extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(getApplicationContext(), "Se ha creado el nuevo usuario", Toast.LENGTH_LONG).show();
            Intent LoginActivity = new Intent(getApplicationContext(), com.example.cesarucci.ticketsv2.LoginActivity.class);
            startActivity(LoginActivity);
        }
    }
}
